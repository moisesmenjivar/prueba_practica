El directorio MAPSPrueba contiene un proyecto Android en el cual se ha desarrollado el ejercicio propuesto para esta prueba.
Consideraciones:
Se ha utilizado java para desarrollar el ejercicio.
Se utiliza JSON para simular cada evento.
Se simula una smartCard por medio de SQLite.
Se simula una pila que permite guardar unicamente 40 registros.
    Solo se pueden agregar elementos al final de la lista.
    Solo se puede escribir en la smartCard un evento a la vez.
    No se pueden editar los elementos que existen el la pila, para esto debe agregarse a la pila un nuevo elemento con las correcciones.
    La lista puede ser recorrida en cualquier dirección, para la implementación y calculos de las notas solo se recorre en una dirección(no es necesario recorrer la lista en varas direcciones para esta implementación)    
Cada evento esta limitado para que se puedan hacer solamente 5 registros por eventos.
Cada clase y metodo utilizado contiene la documentación respectiva necesaria para tener una explicación clara de lo que hace cada componente.

La implementación permite:
    Agregar información de asistencia.
    Agregar notas de examenes y evaluaciones a cada materia ingresados por el usuario.
    Cuando se agrega una correción de nota o evaluación, se toma la mas reciente.
    Se puede extraer de forma individual las activiades y notas de cada materia.
    No se almacenan mas de 5 datos por evento.


Versión de Android Studio utilizada: 3.1.3
compileSdkVersion 27
minSdkVersion 15
targetSdkVersion 27
gradle-4.9-rc-1

JDK 1.8.0_171

En la raiz de este directorio se encuentra el archivo app-debug.apk para instalar en dispositivos android y observar la implementación.





