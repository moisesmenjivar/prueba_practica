package com.example.mapsprueba;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import java.io.IOException;
/**
 * @author: Moy
 * @version: 05/08/2018
 * Esta es la clase con la cual inicia la ejecución de la app
 */
public class MainActivity extends AppCompatActivity {
    Button btn;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn=(Button)findViewById(R.id.buttonGo);
        context=this;
        readSmartCardClick();

    }

    /**
     @author Moy
     Simula la conexión de una smartCard y crea una base de datos SQLite si no existe
     **/
    public void  readSmartCardClick(){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardDB objBB= new CardDB(context);

                try {
                    objBB.createDataBase();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(context, OptionsSmartCard.class);
                startActivity(i);
            }
        });
    }
}

