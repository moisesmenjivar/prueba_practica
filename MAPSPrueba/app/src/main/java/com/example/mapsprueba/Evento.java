package com.example.mapsprueba;

import org.json.JSONException;
import org.json.JSONObject;
/**
 * @author: Moy
 * @version: 05/08/2018
 * Esta es la clase que maneja los eventos asignado al objeto JSON cada registro que se ejecuata en cada evento
 */
public class Evento {
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param asistencia Es la asistencia de un alumno
     * @param event objeto JSON en el que se debe asignar la asistencia
     * @return  event, el evento con la sistencia cargada
     * Agrega o quita la asistencia mientras el evento aun no se ha guardado en la smartCard
     **/
    JSONObject  addAsistencia(double asistencia,JSONObject event){
        if(event.isNull("asistencia")){

        }else{
            event.remove("asistencia");
        }
        try {
            event.put("asistencia",asistencia);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return event;
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param materia Es la materia a la cual se asignara la nota del examen
     * @param notaExamen Es la nota que se asignara al examen de la materia
     * @return  event, el evento con la nota del examen asignada a la materia
     * Agrega o quita la nota del examen de la materia mientras el evento aun no se ha guardado en la smartCard
     **/
    JSONObject addNotaExamen(String materia,double notaExamen,JSONObject event){
        if(event.isNull(materia+"_examen")){

        }else{
            event.remove(materia+"_examen");
        }
        try {
            event.put(materia+"_examen",notaExamen);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return event;
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param materia Es la materia a la cual se asignara la nota de la evaluación
     * @param notaMateria Es la nota que se asignara a la evaluación de la materia
     * @return  event, el evento con la nota de la evaluación asignada a la materia
     * Agrega o quita la nota de la evaluación de la materia mientras el evento aun no se ha guardado en la smartCard
     **/
    JSONObject addNotaEvaluacion(String materia,double notaMateria,JSONObject event){
        if(event.isNull(materia+"_evaluacion")){

        }else{
            event.remove(materia+"_evaluacion");
        }
        try {
            event.put(materia+"_evaluacion",notaMateria);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return event;
    }
}