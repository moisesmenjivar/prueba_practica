package com.example.mapsprueba;

/**
 * @author: Moy
 * @version: 05/08/2018
 * Esta clase permite manejar los spinner que se cargan en las pantallas
 */
public class SpinnerObject {
	private int databaseId;
	private String databaseIdS;
	private String databaseValue;


	/**
	 * @author: Moy
	 * @version: 05/08/2018
	 * @param databaseId, el valor para inicializar la variable databaseId
	 * @param databaseValue, el valor para inicializar la variable databaseValue
	 * Inicializa las variables databaseId,databaseValue
	 */
	public SpinnerObject(int databaseId, String databaseValue) {
		this.databaseId = databaseId;
		this.databaseValue = databaseValue;
	}
	/**
	 * @author: Moy
	 * @version: 05/08/2018
	 * @param databaseIdS, el valor para inicializar la variable databaseIdS
	 * @param databaseValue, el valor para inicializar la variable databaseValue
	 * Inicializa las variables databaseIdS,databaseValue
	 */
	public SpinnerObject(String databaseIdS, String databaseValue) {
		this.databaseIdS = databaseIdS;
		this.databaseValue = databaseValue;
	}


	//Estos metodos obtienen y devuelven los valores de las variables asignadas y solicitadas respectivamente
	public int getId() { return databaseId; }
	public String getIdString() { return databaseIdS; }
	public String getValue()  {
		return databaseValue;
	}
	@Override
	public String toString() {
		return databaseValue;
	}
}