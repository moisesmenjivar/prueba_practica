package com.example.mapsprueba;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author: Moy
 * @version: 05/08/2018
 * Esta clase gestiona los procesos relacionados con la smartCard(una base de datos SQLite que simula la smartCard)
 **/
public class Process {

    public SQLiteDatabase conexOpen;
    private  Context ccontexto;
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Abre una conexión con la smartCard
     */
    public void conectarCardDB(Context contexto) {
        desConectarCardDB();
        CardDB objBB = new CardDB(ccontexto);
        objBB.openDataBase();
        this.conexOpen = objBB.myDataBase;// save the conex

    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Desconecta la smartCard
     */
    public void desConectarCardDB() {
        if (this.conexOpen != null){
            this.conexOpen.close();
        }
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param context, es el contexto de la activity que esta llamando a este metodo
     * @return lastAsistencia, la ultima asistencia asignada
     * Retorma la ultima asistencia asignada en la smartCard
     */
    public double getLastAsistencia(Context context){
        double lastAsistencia=0;
        JSONObject jsonObj=new JSONObject();
        conectarCardDB(context);
        Cursor cursor = conexOpen.rawQuery("SELECT * FROM events ORDER BY event DESC LIMIT 1", null);
        if (cursor.moveToFirst()) {
            do {
                try {
                    jsonObj = new JSONObject(cursor.getString(1));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    lastAsistencia=jsonObj.getDouble("asistencia");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        desConectarCardDB();
        return  lastAsistencia;
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param event, el evento que se ha de insertar en la pila de la smartCard
     * @param context, es el contexto de la activity que esta llamando a este metodo
     * Inserta en la smartCard, simulando una pila
     */
    public void insertPila(JSONObject event,Context context){
        int countEvents=0;
        int minEvents=0;
        conectarCardDB(context);
        Cursor cursor = conexOpen.rawQuery("SELECT count(event) AS cuenta,MIN(event) AS minimo FROM events", null);
        if (cursor.moveToFirst()) {
            do {
                countEvents=cursor.getInt(0);
                minEvents=cursor.getInt(1);
            } while (cursor.moveToNext());
        }
        if(countEvents<40){
            ContentValues newValues = new ContentValues();
            newValues.put("datos"  , ""+event);
            conexOpen.insert("events", null, newValues);
        }else{
            conexOpen.delete("events", "event="+minEvents, null);
            ContentValues newValues = new ContentValues();
            newValues.put("datos"  , ""+event);
            conexOpen.insert("events", null, newValues);
        }
        desConectarCardDB();
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param context, es el contexto de la activity que esta llamando a este metodo
     * @return htmlTable, una tabla html conteniendo las notas
     * Genera una tabla html con los calculos de todas las notas registradas en la smartCard
     */
    public String showNotas(Context context){
        String htmlTable="";
        Boolean datos=false;
        conectarCardDB(context);

        double promedio=0;
        double asistencia=0;

        double eva_salud=0;
        double exa_salud=0;

        double eva_nutricion=0;
        double exa_nutricion=0;

        double eva_arte=0;
        double exa_arte=0;

        double eva_educacion=0;
        double exa_educacion=0;

        double eva_autoestima=0;
        double exa_autoestima=0;

        double eva_empleabilidad=0;
        double exa_empleabilidad=0;

        double eva_servcliente=0;
        double exa_servcliente=0;

        double eva_mercadeo=0;
        double exa_mercadeo=0;


        Cursor cursor = conexOpen.rawQuery("SELECT datos FROM events", null);
        htmlTable+="<table id=\"customers\">" +
                "<tr>" +
                "<th>Capacitación</th>" +
                "<th>Evaluación</th>" +
                "<th>Exámen</th>" +
                "<th>Nota</th>" +
                "</tr>";

        if (cursor.moveToFirst()) {
            do {
                JSONObject rowObject=new JSONObject();
                datos=true;
                try {
                    rowObject = new JSONObject(cursor.getString(0));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(rowObject.isNull("asistencia")){

                }else{
                    try {
                        asistencia=rowObject.getDouble("asistencia");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("salud_evaluacion")){

                }else {
                    try {
                        eva_salud = rowObject.getDouble("salud_evaluacion");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(rowObject.isNull("salud_examen")){

                }else {
                    try {
                        exa_salud = rowObject.getDouble("salud_examen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(rowObject.isNull("nutricion_evaluacion")){

                }else {
                    try {
                        eva_nutricion = rowObject.getDouble("nutricion_evaluacion");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("nutricion_examen")){

                }else {
                    try {
                        exa_nutricion = rowObject.getDouble("nutricion_examen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("arte_evaluacion")){

                }else {
                    try {
                        eva_arte = rowObject.getDouble("arte_evaluacion");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("arte_examen")){

                }else {
                    try {
                        exa_arte = rowObject.getDouble("arte_examen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("educacion_evaluacion")){

                }else {
                    try {
                        eva_educacion = rowObject.getDouble("educacion_evaluacion");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("educacion_examen")){

                }else {
                    try {
                        exa_educacion=rowObject.getDouble("educacion_examen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(rowObject.isNull("autoestima_evaluacion")){

                }else {
                    try {
                        eva_autoestima = rowObject.getDouble("autoestima_evaluacion");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    exa_autoestima=rowObject.getDouble("autoestima_examen");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(rowObject.isNull("empleabilidad_evaluacion")){

                }else {
                    try {
                        eva_empleabilidad = rowObject.getDouble("empleabilidad_evaluacion");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("empleabilidad_examen")){

                }else {
                    try {
                        exa_empleabilidad = rowObject.getDouble("empleabilidad_examen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("empleabilidad_examen")){

                }else {
                    try {
                        exa_empleabilidad = rowObject.getDouble("empleabilidad_examen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("servicio_cliente_evaluacion")){

                }else {
                    try {
                        eva_servcliente = rowObject.getDouble("servicio_cliente_evaluacion");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("servicio_cliente_examen")){

                }else {
                    try {
                        exa_servcliente = rowObject.getDouble("servicio_cliente_examen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("mercadeo_evaluacion")){

                }else {
                    try {
                        eva_mercadeo = rowObject.getDouble("mercadeo_evaluacion");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(rowObject.isNull("mercadeo_examen")){

                }else {
                    try {
                        exa_mercadeo = rowObject.getDouble("mercadeo_examen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } while (cursor.moveToNext());
        }else{
            htmlTable+="<tr>"+
                    "<td colspan=\"4\">No hay datos para mostrar</td>"+
                    "</tr>";
        }
        if(datos){
            promedio=(((((eva_salud+exa_salud)/2)+((eva_nutricion+exa_nutricion)/2)+((eva_arte+exa_arte)/2)+((eva_educacion+exa_educacion)/2)+((eva_autoestima+exa_autoestima)/2)+((eva_empleabilidad+exa_empleabilidad)/2)+((eva_servcliente+exa_servcliente)/2)+((eva_mercadeo+exa_mercadeo)/2))+((asistencia/40)*10))/9);
            htmlTable+="<tr>" +
                    "<td>Salud</td>" +
                    "<td>"+eva_salud+"</td>" +
                    "<td>"+exa_salud+"</td>"+
                    "<td>"+((eva_salud+exa_salud)/2)+"</td>"+
                    "</tr>";
            htmlTable+="<tr>" +
                    "<td>Nutrición</td>" +
                    "<td>"+eva_nutricion+"</td>" +
                    "<td>"+exa_nutricion+"</td>"+
                    "<td>"+((eva_nutricion+exa_nutricion)/2)+"</td>"+
                    "</tr>";
            htmlTable+="<tr>" +
                    "<td>Arte</td>" +
                    "<td>"+eva_arte+"</td>" +
                    "<td>"+exa_arte+"</td>"+
                    "<td>"+((eva_arte+exa_arte)/2)+"</td>"+
                    "</tr>";
            htmlTable+="<tr>" +
                    "<td>Educación</td>" +
                    "<td>"+eva_educacion+"</td>" +
                    "<td>"+exa_educacion+"</td>"+
                    "<td>"+((eva_educacion+exa_educacion)/2)+"</td>"+
                    "</tr>";
            htmlTable+="<tr>" +
                    "<td>Autoestima</td>" +
                    "<td>"+eva_autoestima+"</td>" +
                    "<td>"+exa_autoestima+"</td>"+
                    "<td>"+((eva_autoestima+exa_autoestima)/2)+"</td>"+
                    "</tr>";
            htmlTable+="<tr>" +
                    "<td>Empleabilidad</td>" +
                    "<td>"+eva_empleabilidad+"</td>" +
                    "<td>"+exa_empleabilidad+"</td>"+
                    "<td>"+((eva_empleabilidad+exa_empleabilidad)/2)+"</td>"+
                    "</tr>";
            htmlTable+="<tr>" +
                    "<td>Serv. Cliente</td>" +
                    "<td>"+eva_servcliente+"</td>" +
                    "<td>"+exa_servcliente+"</td>"+
                    "<td>"+((eva_servcliente+exa_servcliente)/2)+"</td>"+
                    "</tr>";
            htmlTable+="<tr>" +
                    "<td>Mercadeo</td>" +
                    "<td>"+eva_mercadeo+"</td>" +
                    "<td>"+exa_mercadeo+"</td>"+
                    "<td>"+((eva_mercadeo+exa_mercadeo)/2)+"</td>"+
                    "</tr>";
            htmlTable+="<tr>" +
                    "<td colspan=\"3\">Asistencia      "+(int)asistencia+" de 40 </td>"+
                    "<td>"+((asistencia/40)*10)+"</td>"+
                    "</tr>";
            htmlTable+="<tr>" +
                    "<td colspan=\"3\">Promedio final</td>"+
                    "<td>"+(double)Math.round(promedio * 100d) / 100d+"</td>"+
                    "</tr>";
        }
        desConectarCardDB();
        htmlTable+="</table>" ;
        return htmlTable;
    }
}
