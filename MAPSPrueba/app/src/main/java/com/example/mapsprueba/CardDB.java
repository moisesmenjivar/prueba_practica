package com.example.mapsprueba;

import java.io.File;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.OutputStream;

        import android.app.ProgressDialog;
        import android.content.Context;
        import android.database.SQLException;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteException;
        import android.database.sqlite.SQLiteOpenHelper;
        import android.os.AsyncTask;
        import android.util.Log;
/**
 * @author: Moy
 * @version: 05/08/2018
 * Esta es la clase que lee el archivo cardDB.db de los assets para generar la base de datos
 * Contiene los metodos necesarios para la gestión de la base de datos que simula una smartCard
 */

    public class CardDB extends SQLiteOpenHelper {
    private static String DB_PATH = "/data/data/com.example.mpasprueba/databases/";
    private static String DB_NAME = "cardDB.db";
    public SQLiteDatabase myDataBase;
    private static File DATABASE_FILE;
    public static final int DATABASE_VERSION = 1;


    private final Context myContext;

    public CardDB(Context contexto) {
        super(contexto, DB_NAME, null, DATABASE_VERSION);
        this.myContext = contexto;
    }

    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param db Es una instancia de la base de datos
     **/
    @Override
    public void onCreate(SQLiteDatabase db) {
        // No hacemos nada aqui, porque copiamos la base de datos de assets
    }

    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param db Es una instancia de la base de datos
     * @param oldVersion el número de la versión anterior de la base de datos
     * @param newVersion el número de la versión nueva de la base de datos
     **/
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Cuando haya cambios en la estructura deberemos maejarlo aquí
        updateDataBaseAsync updateDataBase = new updateDataBaseAsync(myContext,db,oldVersion,newVersion);
        updateDataBase.execute();
    }

    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param db Es una instancia de la base de datos
     * @param oldVersion el número de la versión anterior de la base de datos
     * @param newVersion el número de la versión nueva de la base de datos
     **/
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //codigo para cuando se ponga una version anterior de la DB
        //Log.i("accion:","Intenta bajar de versión");
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param db Es una instancia de la base de datos
     * @param oldVersion el número de la versión anterior de la base de datos
     * @param newVersion el número de la versión nueva de la base de datos
     **/
    private void updateDataBase(Context contexto,SQLiteDatabase db,int oldVersion,int newVersion){
        //aqui se debe manejar las actualizaciones a la base de datos
    }

    /**
     * @author: Moy
     * @version: 05/08/2018
     * Verifica si existe la base de datos o no, si no existe llama al metodo que debe crearla
     **/
    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (dbExist) {
            // Si existe, no haemos nada!
            getWritableDatabase();

        }else{
            copyDataBaseAsync copyDataBase = new copyDataBaseAsync(myContext);
            copyDataBase.execute();
        }
    }

    /**
     * @author: Moy
     * @version: 05/08/2018
     * @return existe, true or false para saber si la base de datos existe o no
     * Verifica si existe la base de datos o no
     **/
    public boolean checkDataBase() {
        boolean existe= false;
        String myPath = DB_PATH + DB_NAME;
        File file = new File(myPath);
        existe=file.exists();
        return  existe;
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Abre una instancia de la base de datos y la asigna a myDataBase
     **/
    public void openDataBase() throws SQLException {
        // Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null,SQLiteDatabase.OPEN_READWRITE);
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Cierra la instancia de la base de datos
     **/
    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();

        super.close();
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Lee el archivo de los assets que contiene la base de datos y crea la base con las instrucciones del archivo leido
     **/
    private void copyDataBase() throws IOException {
        getReadableDatabase();
        OutputStream databaseOutputStream = new FileOutputStream("" + DB_PATH	+ DB_NAME);
        InputStream databaseInputStream;
        //InputStream databaseInputStream2=new FileInputStream()
        byte[] buffer = new byte[1024];
        int length;
        databaseInputStream = myContext.getAssets().open(DB_NAME);
        while ((length = databaseInputStream.read(buffer)) > 0) {
            databaseOutputStream.write(buffer);
        }

        databaseInputStream.close();
        databaseOutputStream.flush();
        databaseOutputStream.close();
        setDatabaseVersion();
        //Log.i("Acción", "Copio la base de datos");
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Asigna la versión de la base de datos
     **/
    private void setDatabaseVersion() {
        DATABASE_FILE = myContext.getDatabasePath(DB_NAME);
        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(DATABASE_FILE.getAbsolutePath(), null,SQLiteDatabase.OPEN_READWRITE);
            db.execSQL("PRAGMA user_version = " + DATABASE_VERSION);
        } catch (SQLiteException e ) {
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
    }

    /**
     * @author: Moy
     * @version: 05/08/2018
     * Esta clase maneja los procesos para crear la base de datos de forma asincrona
     **/
    public class copyDataBaseAsync extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        Context contexto;

        public copyDataBaseAsync(Context contexto){
            this.contexto = contexto;
        }
        protected String doInBackground(String... params) {
            try {
                copyDataBase();
                //irLogin();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(contexto,"","Copiando la Base de datos...",true);
        }

    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Esta clase maneja los procesos para actualizar la base de datos de forma asincrona
     **/
    public class updateDataBaseAsync extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        Context contexto;
        SQLiteDatabase db1;
        int oVersion;
        int nVersion;

        public updateDataBaseAsync(Context contexto,SQLiteDatabase db,int oldversion,int newversion){
            this.contexto = contexto;
            this.db1=db;
            this.oVersion=oldversion;
            this.nVersion=newversion;
        }
        protected String doInBackground(String... params) {
            updateDataBase(this.contexto,db1,oVersion,nVersion);
            //irLogin();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(contexto,"","Actualizando la Base de datos...",true);
        }
    }

}

