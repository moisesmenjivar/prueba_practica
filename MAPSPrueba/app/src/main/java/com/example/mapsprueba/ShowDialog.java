package com.example.mapsprueba;


import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;
/**
 * @author: Moy
 * @version: 05/08/2018
 * Esta clase permite inflar las pantallas solicitadas
 */
public class ShowDialog {
    Dialog customDialog=null;
    TextView textViewTitulo;

    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param context, es el contexto desde el cual se llama a este metodo
     * @param view, es la vista que se desea cargar
     * @param titulo, el titulo que se asignara a la vista
     * @return customDialog, es la vista que se devuelve, customizada
     * Permite cargar una vista customizada
     */
    public Dialog ShowDialog(Context context , int view, String titulo){

        customDialog = new Dialog(context, R.style.Theme_AppCompat_DayNight);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(view);

        customDialog.show();
        textViewTitulo  = (TextView) customDialog.findViewById(R.id.textviewTitulo);
        textViewTitulo.setText(titulo);
        return customDialog;
       }
}
