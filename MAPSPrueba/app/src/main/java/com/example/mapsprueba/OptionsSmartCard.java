package com.example.mapsprueba;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
/**
 * @author: Moy
 * @version: 05/08/2018
 * Esta es la clase que maneja las opciones que tiene una smartCard, es la clase desde donde se ejecutan todas las acciones de la smartCard
 * Opciones de una smart card: ver notas, registrar asistencia, registrar nota examen, registrar nota evaluación, guardar evento, cancelar el evento
 */
public class OptionsSmartCard extends AppCompatActivity {

    Evento objEvento;
    Process objProcess;
    double lastAsistencia=0;
    double newAsistencia=0;
    Button btnAsistencia;
    Button btnExamen;
    Button btnEvaluacion;
    Button btnGuardarEvento;
    Button btnShowNotas;
    Button btnSalirEvento;


    Spinner spMateriasExa;
    Spinner spMateriasEva;
    TextView etNotaExamen;
    TextView etNotaEvaluacion;
    TextView tvCountReg;
    TextView tvShowReg;
    ShowDialog objDialogExaEva=null;
    ShowDialog objDialogShowNotas=null;
    Dialog dialogExamen=null;
    Dialog dialogEvaluacion=null;
    Dialog dialogShowNotas=null;
    WebView wvShowNotas;
    Context context;
    JSONObject JSONEvento;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.options_smart_card);
            context=this;

            btnShowNotas=(Button)findViewById(R.id.buttonShowNotas);

            btnAsistencia=(Button)findViewById(R.id.buttonAsistencia);
            btnExamen=(Button)findViewById(R.id.buttonExamen);
            btnEvaluacion=(Button)findViewById(R.id.buttonEvaluacion);
            btnGuardarEvento=(Button)findViewById(R.id.buttonGuardarEvento);
            btnSalirEvento  =(Button)findViewById(R.id.buttonSalirEvento);

            tvCountReg=(TextView)findViewById(R.id.textCountRegistros);
            tvShowReg=(TextView)findViewById(R.id.showReg);

            objEvento=new Evento();
            JSONEvento=new JSONObject();

            objProcess=new Process();
            lastAsistencia=objProcess.getLastAsistencia(this);
            asistenciaAdd();
            examenAdd();
            evaluacionAdd();
            setBtnGuardarEvento();
            setBtnSalirEvento();
            showNotas();
            tvCountReg.setText("Registros: "+JSONEvento.length());
        }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Muestra las notas que se han guardado en la smartCard
     */
    public void  showNotas(){
        btnShowNotas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objDialogShowNotas = new ShowDialog();
                dialogShowNotas = objDialogShowNotas.ShowDialog(context, R.layout.dialog_show_notas, "Boleta de Notas");
                wvShowNotas = (WebView) dialogShowNotas.findViewById(R.id.wvShowNotas);
                String htmlWebView =
                        "<html>" +
                                "<head>"+
                                "<style>#customers { font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif; border-collapse: collapse;  width: 100%; }"+
                                "#customers td, #customers th { border: 1px solid #ddd; padding: 8px; }"+
                                "#customers tr:nth-child(even){background-color: #f2f2f2;}"+
                                "#customers tr:hover {background-color: #ddd;}"+
                                "#customers th {padding-top: 12px;padding-bottom: 12px;text-align: left;background-color:  #4CAF50;   color: white; }"+
                                "</style>"+

                                "</head><body>";
                htmlWebView+=objProcess.showNotas(OptionsSmartCard.this);
                htmlWebView+="</body></html>";

                wvShowNotas.loadData(htmlWebView, "text/html; charset=utf-8", null);
                cancelarShowNotas();
            }
        });
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Cierra la pantalla que muestra las notas que se han guardado en la smartCard
     */
    private void  cancelarShowNotas(){
        ((Button) dialogShowNotas.findViewById(R.id.buttonCancelarShowNotas)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogShowNotas.dismiss();
            }
        });
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Llama al procedimiento que permite asignar la asistencia
     */
    public void  asistenciaAdd(){
        btnAsistencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(JSONEvento.length()>=5){
                    toastMje("No puede ingresar mas de 5 registros por evento");
                }else{
                    newAsistencia=0;
                    //getAsistenciaDB();
                    newAsistencia=lastAsistencia+1;
                    if(newAsistencia>40){
                       newAsistencia=40;
                    }
                    JSONEvento=objEvento.addAsistencia(newAsistencia,JSONEvento);
                    toastMje("Asistencia lista para guardar");
                    tvCountReg.setText("Registros: "+JSONEvento.length());
                    tvShowReg.setText("Evento: "+JSONEvento);
                }


            }
        });
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Llama al procedimiento que permite asignar la nota al examen de una materia
     */
    public void  examenAdd(){
        btnExamen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(JSONEvento.length()>=5){
                    toastMje("No puede ingresar mas de 5 registros por evento");
                }else {
                    objDialogExaEva = new ShowDialog();
                    dialogExamen = objDialogExaEva.ShowDialog(context, R.layout.dialog_examen_evaluacion, "Examen");
                    spMateriasExa = (Spinner) dialogExamen.findViewById(R.id.spinnerMateria);
                    loadSpinnerObject(spMateriasExa);
                    aceptarExamen();
                    cancelar(dialogExamen);
                    //toastMje("Examen listo para guardar");
                }
            }
        });
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Llama al procedimiento que permite asignar la nota a la evaluación de una materia
     */
    public void  evaluacionAdd(){
        btnEvaluacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(JSONEvento.length()>=5){
                    toastMje("No puede ingresar mas de 5 registros por evento");
                }else{
                objDialogExaEva=new ShowDialog();
                dialogEvaluacion=objDialogExaEva.ShowDialog(context,  R.layout.dialog_examen_evaluacion,"Evaluación");
                spMateriasEva=(Spinner) dialogEvaluacion.findViewById(R.id.spinnerMateria);
                loadSpinnerObject(spMateriasEva);
                //toastMje("Evaluacion listo para guardar");
                aceptarEvaluacion();
                cancelar(dialogEvaluacion);
                }
            }
        });
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param mje, es el mensaje que se desea mostrar en un toast
     * Muestra en pantalla un mensaje toast
     */
    void toastMje(String mje){
        Toast.makeText(this,mje, Toast.LENGTH_LONG).show();
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Maneja las validaciones para asignar una nota de examen a una materia
     */
     private void  aceptarExamen(){
          ((Button) dialogExamen.findViewById(R.id.buttonGuardarNota)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpinnerObject       spdata      = (SpinnerObject)       spMateriasExa.getItemAtPosition(spMateriasExa.getSelectedItemPosition());
                etNotaExamen=(TextView) dialogExamen.findViewById(R.id.editTextNota);

                boolean isnumber=false;
                try {
                    double numVal = Double.valueOf(etNotaExamen.getText().toString());
                    isnumber=true;
                } catch (NumberFormatException e){
                    isnumber=false;
                }


                if(spdata.getIdString().equals("0")){
                    toastMje("No ha seleccionado la materia");
                }else if(etNotaExamen.getText().toString().equals("")){
                    toastMje("No ha ingresado la nota del examen");
                }else if(!isnumber){
                    toastMje("Ingrese una nota valida");
                }else if(Double.parseDouble(etNotaExamen.getText().toString())>10 || Double.parseDouble(etNotaExamen.getText().toString())<0){
                    toastMje("Ingrese una nota valida");
                }else{
                    JSONEvento=objEvento.addNotaExamen(spdata.getIdString(),Double.parseDouble(etNotaExamen.getText().toString()),JSONEvento);
                    dialogExamen.dismiss();
                    toastMje("Guardando examen");
                    tvCountReg.setText("Registros: "+JSONEvento.length());
                    tvShowReg.setText("Evento: "+JSONEvento);
                }

            }
        });
        }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Maneja las validaciones para asignar una nota de evaluación a una materia
     */
    private void  aceptarEvaluacion(){
        ((Button) dialogEvaluacion.findViewById(R.id.buttonGuardarNota)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpinnerObject       spdata      = (SpinnerObject)       spMateriasEva.getItemAtPosition(spMateriasEva.getSelectedItemPosition());
                etNotaEvaluacion=(TextView) dialogEvaluacion.findViewById(R.id.editTextNota);

                boolean isnumber=false;
                try {
                    double numVal = Double.valueOf(etNotaEvaluacion.getText().toString());
                    isnumber=true;
                } catch (NumberFormatException e){
                    isnumber=false;
                }

                if(spdata.getIdString().equals("0")){
                    toastMje("No ha seleccionado la materia");
                }else if(etNotaEvaluacion.getText().toString().equals("")){
                    toastMje("No ha ingresado la nota de la evaluación");
                }else if(!isnumber){
                    toastMje("Ingrese una nota valida");
                }else if(Double.parseDouble(etNotaEvaluacion.getText().toString())>10 || Double.parseDouble(etNotaEvaluacion.getText().toString())<0){
                    toastMje("Ingrese una nota valida");
                }else{
                    JSONEvento=objEvento.addNotaEvaluacion(spdata.getIdString(),Double.parseDouble(etNotaEvaluacion.getText().toString()),JSONEvento);
                    dialogEvaluacion.dismiss();
                    toastMje("Guardando evaluación");
                    tvCountReg.setText("Registros: "+JSONEvento.length());
                    tvShowReg.setText("Evento: "+JSONEvento);
                }
            }
        });
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param dialog, la pantalla que se ha de cancelar
     * Cancela las pantallas que recibe como parametro
     */
    private void  cancelar(final Dialog dialog){
        ((Button) dialog.findViewById(R.id.buttonCancelarEvento)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Llama al mecanismo que permite guardar un evento completo en la smartCard
     */
    public void  setBtnGuardarEvento(){
        btnGuardarEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(JSONEvento.length()==0){
                    //Log.i("eventoGuardando","El JSON va nulo, no se guarda en la base");
                }else{
                    objProcess.insertPila(JSONEvento,OptionsSmartCard.this);
                    //Log.i("eventoGuardando",""+JSONEvento);
                    toastMje("Evento guardado en la tarjeta");
                }
                startActivity(new Intent(OptionsSmartCard.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            }
        });
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * Permite salir de la pantalla de menu de la smartCard
     */
    public void  setBtnSalirEvento(){
        btnSalirEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OptionsSmartCard.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            }
        });
    }
    /**
     * @author: Moy
     * @version: 05/08/2018
     * @param sp, el spinner al cual se cargara la lista
     * Carga un spinner con la lista de materias
     */
    void loadSpinnerObject(Spinner sp) {
        List<SpinnerObject> list= getListMarerias();
        ArrayAdapter<SpinnerObject> adap = new ArrayAdapter<SpinnerObject>(context, android.R.layout.simple_spinner_item, list);
        adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adap);
    }

    /**
     * @author: Moy
     * @version: 05/08/2018
     * @return  lista, lista de materias
     * Genera un array con todas las materias
     */
    public List<SpinnerObject> getListMarerias(){
        List<SpinnerObject> lista= new ArrayList<SpinnerObject>();
        lista.add(new SpinnerObject("0", "--Seleccione--"));
        lista.add(new SpinnerObject("salud", "salud"));
        lista.add(new SpinnerObject("nutricion", "Nutrición"));
        lista.add(new SpinnerObject("arte", "Arte"));
        lista.add(new SpinnerObject("educacion", "Educación"));
        lista.add(new SpinnerObject("autoestima", "Autoestima"));
        lista.add(new SpinnerObject("empleabilidad", "Empleabilidad"));
        lista.add(new SpinnerObject("servicio_cliente", "Servicio al cliente"));
        lista.add(new SpinnerObject("mercadeo", "Mercadeo"));

        return lista;
    }

}
